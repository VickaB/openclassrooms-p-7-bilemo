<?php


namespace App\Exception;



use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;


class CustomHttpException extends HttpException implements HttpExceptionInterface
{
	private array $headers;
	protected $message;

	public function __construct(int $statusCode, string $message = null, Throwable $previous = null, array $headers = [], ?int $code = 0)
	{
		parent::__construct($statusCode, $message, $previous, $headers, $code);

	}

	public function getCustomMessage()
	{
		return $this->message;
	}

	/**
	 * @return int
	 */
	public function getStatusCode(): int
	{
		return $this->getCode();
	}

	/**
	 * @return array
	 */
	public function getHeaders(): array
	{
		return $this->headers;
	}

}