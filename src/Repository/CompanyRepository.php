<?php

namespace App\Repository;

use App\Entity\Company;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends AbstractRepository
{
	private int $limit;

    public function __construct(ManagerRegistry $registry, int $limit)
    {
        parent::__construct($registry, Company::class);
	    $this->limit = $limit;
    }

	public function GetPaginatedCompanies($term, $order,$page)
	{
		$query = $this->createQueryBuilder('c')
			->setFirstResult(($page - 1) * $this->limit)
			->setMaxResults($this->limit)
			->orderBy('c.id', $order = $order?$order:'ASC');
		if ($term) {

			$query->where('c.name LIKE ?1')
				->setParameter(1, '%'.$term.'%')
			;
		}
		$query->getQuery();

		return $this->paginate($query,"companies",$page,$this->limit,count($this->findAll()));
	}

    // /**
    //  * @return Company[] Returns an array of Company objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Company
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
