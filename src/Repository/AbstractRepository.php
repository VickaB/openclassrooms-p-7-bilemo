<?php


namespace App\Repository;



use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class AbstractRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry, string $entity = "")
	{
		parent::__construct($registry, $entity);
	}

	protected function paginate(QueryBuilder $qb,$resources_name, $page, $limit,$total): array
	{
		$paginator =new Paginator($qb);
		try {
			return array(
				'resources' => $paginator->getIterator()->getArrayCopy(),
				'resources_name' => $resources_name,
				'page' => $page,
				'totalItem' => $total,
				'totalQuery' => $paginator->count(),
				'limit' => $limit
			);
		} catch (Exception $e) {
			return ['Error' => $e->getMessage(), 'Status' => Response::HTTP_INTERNAL_SERVER_ERROR,'Header' => [
				'Content-Type' => 'application/json'
			]];
		}
	}
}