<?php


namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

	/**
	 * @Route("/", name="index", methods={"GET"})
	 */
	public function index() : Response
	{
		return $this->render('default.html.twig');
	}

//	/**
//	 * @Route("/", name="home")
//	 */
//	public function index(Stopwatch $stopwatch, CacheInterface $cache)
//	{
//		$stopwatch->start('calcul-long');
//
//		// On imagine un calcul ou un traitement long
//		$resultatCalcul = $cache->get('calcul', function (){
//			return $this->fonctionQuiPrendDuTemps();
//		});
//
//		$stopwatch->stop('calcul-long');
//
//		return $this->render('default.html.twig',[
//			'calcul' => $resultatCalcul
//		]);
//	}
//
//	private function fonctionQuiPrendDuTemps(): int
//	{
//		sleep(2);
//
//		return 10;
//	}
}