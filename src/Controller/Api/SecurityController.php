<?php

namespace App\Controller\Api;

use App\Entity\Company;
use App\Entity\User;
use App\Exception\CustomHttpException;
use App\Exception\CustomValidationException;
use App\Service\ViolationHandler;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Areas;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class SecurityController extends AbstractFOSRestController
{
	/**
	 * @Areas({"default","bilmo"})
	 * @OA\Tag(name="Company Users")
	 * @Operation(operationId="register_user")
	 * @OA\Post(path="/companies/{company}/users",description="Register new User for Company",operationId="register_user")
	 * @OA\Parameter(name="company",in="path",description="The Company Id",@OA\Schema(type="integer")
	 * )
	 * @OA\RequestBody(required=true,
	 *       @OA\MediaType(mediaType="application/json",
	 *           @OA\Schema(type="object",
	 *              @OA\Property(property="firstname",description="User firstname",type="string",example="Jane"),
	 *              @OA\Property(property="lastname",description="User lastname",type="string",example="Doe"),
	 *              @OA\Property(property="username",description="User email",type="string",example="janedoe@email.com"),
	 *              @OA\Property(property="email",description="User email",type="string",example="janedoe@email.com"),
	 *              @OA\Property(property="roles",type="array",@OA\Items(type="string",example="ROLE_USER"),description="User role(s)"),
	 *              @OA\Property(property="password",description="User password",type="string",example="MyF4bU!0usP@$s"),
	 *           ))))
	 * )
	 * @OA\Response(response=201,description="Created",
	 *     @OA\JsonContent( type="array",@OA\Items(ref=@Model(type=User::class, groups={"usershow"}))),
	 *     @OA\Link(link="companyUser")
	 * )
	 * @OA\Response(response=400,description="Bad Request",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string",description="status code"),
	 *                     @OA\Property(property="violation field",type="string",description="violation description"),
	 *                     example={"status": "400","lastname": "the field lastname can't be empty"}
	 *                   ))}
	 * )
	 * @OA\Response(response=404,description="Not Found",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="error",type="string", description="violation description"),
	 *                     example={"status": "404", "message": "We couldn't find this Company"}
	 *                   ))}
	 * )
	 * @OA\Response(response=403,description="Forbidden",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string",description="status code"),
	 *                     @OA\Property(property="message",type="string",description="violation description"),
	 *                     example={"status": "403","message": "You don't have access authorization"}
	 *                   ))}
	 * )
	 * @Rest\Post ("/companies/{company}/users", name="register_user")
	 * @Rest\View(statusCode = 201,serializerGroups={"company","usershow"})
	 * @ParamConverter("user", converter="fos_rest.request_body",options={ "validator"={ "groups"="registration" }})
	 * @IsGranted("ROLE_ADMIN")
	 * @param Company $company
	 * @param User $user
	 * @param UserPasswordEncoderInterface $passwordEncoder
	 * @param EntityManagerInterface $entityManager
	 * @param ValidatorInterface $validator
	 * @param ViolationHandler $violationHandler
	 * @return View
	 * @throws CustomValidationException
	 */
	public function register(
		Company $company,
		User $user,
		UserPasswordEncoderInterface $passwordEncoder,
		EntityManagerInterface $entityManager,
		ValidatorInterface $validator,
		ViolationHandler $violationHandler
	): View
	{
		/**
		 * @var User $admin
		 */
		$admin = $this->getUser();
		try {
			if ($admin->getCompany() !== $company ) {
				if (!$this->isGranted("ROLE_SUPER_ADMIN")) {
					throw new CustomHttpException( "You don't have access authorization", Response::HTTP_FORBIDDEN,null);
				}
			}
		}catch (CustomHttpException $e){
			return $this->view($e->getMessage(), Response::HTTP_FORBIDDEN, [
				'Content-Type' => 'application/json'
			]);
		}

		$user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
		$user->setRoles($user->getRoles());
		$user->setCompany($company);
		$user->setCreatedAt(new DateTime("now"));
		$user->setUpdatedAt(new DateTime("now"));
		$user->regenerateUuid();
		$violations = $validator->validate($user, null, ['registration']);
		if(count($violations)) {
			$violationHandler->retrieve($violations);
		}

		$entityManager->persist($user);
		$entityManager->flush();

		return $this->view($user, Response::HTTP_CREATED, ['Location' => $this->generateUrl('company_user', ['company' =>$company->getId(),'user' => $user->getId(), UrlGeneratorInterface::ABSOLUTE_URL])]);
	}

	/**
	 * @Rest\Post("/login", name="login")
	 */
	public function login(): View
	{
		$user = $this->getUser();
		return View::create([
			'email' => $user->getUsername(),
			'roles' => $user->getRoles(),
		]);
	}

	/**
	 * @Areas({"default","bilmo"})
	 * @OA\Tag(name="Auth")
	 * @Operation(operationId="api_login")
	 * @OA\Post(path="/login_check",  operationId="api_login")
	 * @OA\RequestBody(required=true,
	 *       @OA\MediaType(mediaType="application/json",
	 *           @OA\Schema(type="object",
	 *               @OA\Property(property="username", description="User email", type="string", example="janedoe@email.com"),
	 *               @OA\Property(property="password", description="User password", type="string", example="MyF4bU!0usP@$s")))
	 * )
	 * @OA\Response(response=200, description="Ok",
	 *     content={
	 *             @OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="token", type="string", description="JWT access token"),
	 *                     example={"token": "eyJ0eXAiOiJKV1QiLCJhbGc..."}
	 *                 ))}
	 *  )
	 *  @OA\Response(
	 *     response=400,
	 *     description="Bad Request",
	 *     content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string",description="status code"),
	 *                     @OA\Property(property="violation field",type="string",description="violation description"),
	 *                     example={"status": "400","email": "This email is not a valid email"}
	 *                 ))}
	 *  )
	 * @Rest\Post(name="api_login", path="/login_check")
	 * @return View
	 */
	public function api_login(): View
	{
		$user = $this->getUser();
		return View::create([
			'email' => $user->getUsername(),
			'roles' => $user->getRoles(),
		]);
	}


}
