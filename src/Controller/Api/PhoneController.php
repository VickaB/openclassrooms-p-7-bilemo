<?php

namespace App\Controller\Api;

use App\Entity\Phone;
use App\Exception\CustomHttpException;
use App\Repository\PhoneRepository;
use App\Representation\PagerRepresentation;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;

use Nelmio\ApiDocBundle\Annotation\Areas;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * Class PhoneController
 * @package App\Controller\Api
 */
class PhoneController extends AbstractFOSRestController
{


	/**
	 * @Areas({"default","bilmo"})
	 * @OA\Tag(name="Phones")
	 * @Operation(operationId="phones")
	 * @OA\Get(path="/phones",description="Get phones list for users",operationId="phones")
	 * @OA\Parameter(name="page",in="query",description="The Page number",@OA\Schema(type="integer")),
	 * @OA\Parameter(name="sort",in="query",description="The Phone property to order by",@OA\Schema(type="string")),
	 * @OA\Parameter (name="order",in="query",description="The sorting direction",@OA\Schema(type="string")),
	 * @OA\Parameter (name="search",in="query",description="The search term",@OA\Schema(type="string"))
	 * @OA\Response(response=401, description="Unauthorized",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "401", "message": "Expired JWT Token"}))}
	 * )
	 * @OA\Response(response=403,description="Forbidden",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string",description="status code"),
	 *                     @OA\Property(property="message", type="string",description="violation description"),
	 *                     example={"status": "403", "message": "You don't have access authorization"}
	 *                   ) )}
	 * )
	 * @OA\Response(response=200, description="Ok",
	 *      content={@OA\MediaType
	 *                  (mediaType="application/json",
	 *                      @OA\Schema(type="object",
	 *                          @OA\Property(property="metas", type="object",description="Informations about search result",
	 *                          example={
	 *                          "Total Records": "integer",
	 *                          "Total Search Hits": "integer",
	 *                          "Records per page": "integer",
	 *                          "Total Pages": "integer",
	 *                          "Current page": "integer",
	 *                          "Next": "string"
	 *                          }),
	 *                          @OA\Property(property="phones", type="array",description="Phones datas", @OA\Items(ref=@Model(type=Phone::class, groups={"phonelist"})))
	 *                  ))
	 *      }
	 * )
	 * @Rest\Get("/phones", name="phones")
	 * @Rest\View(statusCode = 200,serializerGroups={"phonelist"})
	 * @QueryParam(name="page", requirements="\d+", default=1,description="Page number for pagination")
	 * @QueryParam(name="sort",requirements="name|price|id", default="id",description="Order by property")
	 * @QueryParam(name="order",requirements="asc|desc",default="asc",description="Sort order (asc, desc)")
	 * @QueryParam(name="search",requirements="^\w+$",nullable=true, default=null,description="Search query to look for phone")
	 * @IsGranted("IS_AUTHENTICATED_FULLY", message="Please login to access ressource.")
	 * @param ParamFetcherInterface $paramFetcher
	 * @param PhoneRepository $phoneRepository
	 * @param CacheInterface $cache
	 * @param Request $request
	 * @return View
	 * @throws InvalidArgumentException
	 */
	public function phones(ParamFetcherInterface $paramFetcher, PhoneRepository $phoneRepository, CacheInterface $cache, Request $request): View
	{
		$page = max(1,$paramFetcher->get('page', 1));
		$phonelist = $cache->get('phone-list'.$page,function (ItemInterface $item) use ($phoneRepository, $page, $paramFetcher) {
			$item->expiresAfter(15);
			return $phoneRepository->GetPaginatedPhones(strtolower($paramFetcher->get('search')), $paramFetcher->get('sort'), $paramFetcher->get('order'), $page);
		},1.0);

		return $this->view([(new PagerRepresentation($phonelist, $request->getSchemeAndHttpHost()))->getRepresentation()]);
	}

	/**
	 * @Areas({"default","bilmo"})
	 * @OA\Tag(name="Phones")
	 * @Operation(operationId="phone")
	 * @OA\Get(path="/phones/{phone}",description="Get phone by id for users",operationId="phone")
	 * @OA\Parameter(name="phone",in="path",description="The Phone ID",@OA\Schema(type="integer"))
	 * @OA\Response(response=200,description="OK",
	 *     @OA\JsonContent(ref=@Model(type=Phone::class, groups={"phoneshow"}))
	 * )
	 * @OA\Response(response=404,description="Not Found",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string",description="status code"),
	 *                     @OA\Property( property="error",type="string",description="violation description"),
	 *                     example={"status": "404","lastname": "We couldn't find this Phone"}
	 *                   ))}
	 * )
	 * @OA\Response(response=401, description="Unauthorized",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "401", "message": "Expired JWT Token"}))}
	 * )
	 * @OA\Response(response=403,description="Forbidden",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string",description="status code"),
	 *                     @OA\Property(property="message",type="string",description="violation description"),
	 *                     example={"status": "403","message": "You don't have access authorization"}
	 *                   ))}
	 * )
	 * @Rest\Get("/phones/{phone}", name="phone")
	 * @Rest\View(statusCode = 200, serializerGroups={"phoneshow"})
	 * @IsGranted("ROLE_USER", message="Please login to access ressource.")
	 * @param Phone|null $phone
	 * @param CacheInterface $cache
	 * @return mixed
	 * @throws InvalidArgumentException
	 */
	public function phone(?Phone $phone, CacheInterface $cache)
	{
		if(!$phone){
			throw new CustomHttpException(Response::HTTP_NOT_FOUND, 'We couldn\'t find this Phone.',null, [
				'Content-Type' => 'application/json'
			]);
		}
		return $cache->get('phone'.$phone->getId(),function (ItemInterface $item) use ($phone) {
			$item->expiresAfter(15);
			return $phone;
		},1.0);
	}


}
