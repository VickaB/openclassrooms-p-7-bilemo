<?php

namespace App\Controller\Api;

use App\Entity\Company;
use App\Exception\CustomHttpException;
use App\Repository\CompanyRepository;
use App\Representation\PagerRepresentation;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Areas;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * Class CompanyController
 * @package App\Controller\Api
 */
class CompanyController extends AbstractFOSRestController
{

	private CompanyRepository $repository;

	public function __construct(CompanyRepository $repository)
	{
		$this->repository = $repository;
	}

	/**
	 * @Areas({"bilmo"})
	 * @OA\Tag(name="Bilmo Admin")
	 * @Operation(operationId="getCompanies")
	 * @OA\Get (path="/bilmo/companies", description="Get companies list for super admin")
	 * @OA\Parameter(name="page",in="query",description="The Page number",@OA\Schema(type="integer")),
	 * @OA\Parameter (name="order",in="query",description="The sorting direction",@OA\Schema(type="string")),
	 * @OA\Parameter (name="search",in="query",description="The search term",@OA\Schema(type="string"))
	 * @OA\Response(response=200, description="Ok",
	 *      content={@OA\MediaType
	 *                  (mediaType="application/json",
	 *                      @OA\Schema(type="object",
	 *                          @OA\Property(property="metas", type="object",description="Informations about search result",
	 *                          example={
	 *                          "Total Records": "integer",
	 *                          "Total Search Hits": "integer",
	 *                          "Records per page": "integer",
	 *                          "Total Pages": "integer",
	 *                          "Current page": "integer",
	 *                          "Next": "string"
	 *                          }),
	 *                          @OA\Property(property="companies", type="array",description="Companies datas", @OA\Items(ref=@Model(type=Company::class, groups={"userlist","company"})))
	 *                  ))
	 *      }
	 * )
	 * @OA\Response(response=401, description="Unauthorized",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "401", "message": "We couldn't find JWT Token"}))}
	 * )
	 * @OA\Response(response=403,description="Forbidden",
	 *     content={
	 *             @OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status", type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description" ), example={
	 *                         "status": "403", "message": "You don't have access authorization"}))}
	 * )
	 * @Rest\Get ("/bilmo/companies", name="companies")
	 * @Rest\View (serializerGroups={"superadmin","company"})
	 * @QueryParam(name="page", requirements="\d+", default="1", description="Page number for pagination")
	 * @QueryParam(name="order", requirements="asc|desc", default="asc", description="Sort order (asc, desc)")
	 * @QueryParam(name="search", requirements="[a-zA-Z]+", default=null, nullable=true, description="Search query to look for user")
	 * @IsGranted("ROLE_SUPER_ADMIN", message="You don't have proper access level authorization.")
	 * @param ParamFetcherInterface $paramFetcher
	 * @param Request $request
	 * @param CacheInterface $cache
	 * @return View
	 * @throws InvalidArgumentException
	 */
    public function companies(ParamFetcherInterface $paramFetcher, Request $request, CacheInterface $cache): View
    {
	    $page = max(1,$paramFetcher->get('page', 1));
	    $companies = $cache->get('company-list'.$page,function (ItemInterface $item) use ($page, $paramFetcher) {
		    $item->expiresAfter(15);
	    	return $this->repository->GetPaginatedCompanies(strtolower($paramFetcher->get('search')),$paramFetcher->get('order'),$page);
	    },1.0);
	    return $this->view((new PagerRepresentation($companies, $request->getSchemeAndHttpHost()))->getRepresentation()) ;
    }

	/**
	 * @Areas({"default","bilmo"})
	 * @OA\Tag(name="Company")
	 * @Operation(operationId="getCompanyById")
	 * @OA\Get (path="/companies/{company}", description="Get company by Id for super admin or admin")
	 * @OA\Parameter(name="company", in="path", description="The Company Id", @OA\Schema(type="integer")))
	 * @OA\Response(response=200, description="Ok",
	 *     @OA\JsonContent(ref=@Model(type=Company::class, groups={"superadmin","company"}))
	 * )
	 * @OA\Response(response=404, description="Not Found",
	 *     content={@OA\MediaType(
	 *                 mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status", type="string", description="status code"),
	 *                     @OA\Property(property="error", type="string", description="violation description"), example={"status": "404", "message": "We couldn't find this Company"}))
	 * })
	 * @OA\Response(response=401, description="Unauthorized",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "401", "message": "Expired JWT Token"}))}
	 * )
	 * @OA\Response(response=403, description="Forbidden",
	 *     content={@OA\MediaType(mediaType="application/json",
	 *                 @OA\Schema(
	 *                     @OA\Property(property="status",type="string", description="status code"),
	 *                     @OA\Property(property="message", type="string", description="violation description"),example={"status": "403", "message": "You don't have access authorization"}))}
	 * )
	 * @Rest\Get("/companies/{company}", name="company", requirements={"company"="\d+"})
	 * @Rest\View(statusCode = 200,serializerGroups={"company"})
	 * @Assert\Valid()
	 * @IsGranted("ROLE_ADMIN", message="You don't have proper access level authorization.")
	 * @param Company|null $company
	 * @param CacheInterface $cache
	 * @return Company
	 * @throws InvalidArgumentException
	 */
	public function company(?Company $company, CacheInterface $cache): Company
	{
		if ($company !== $this->getUser()->getCompany() & !$this->isGranted('ROLE_SUPER_ADMIN')){
			throw $this->createAccessDeniedException('You don\'t belong to this Company');
		}
		if(!$company){
			throw new CustomHttpException(Response::HTTP_NOT_FOUND, 'We couldn\'t find this Company.',null, [
				'Content-Type' => 'application/json'
			]);
		}
		return $cache->get('company'.$company->getId(),function (ItemInterface $item) use ($company) {
			$item->expiresAfter(15);
			return $company;
		},1.0);
	}
}
