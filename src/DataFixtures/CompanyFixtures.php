<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CompanyFixtures extends Fixture implements FixtureGroupInterface
{
	private UserPasswordEncoderInterface $passwordEncoder;
	private ContainerInterface $container;

	public function __construct(UserPasswordEncoderInterface $passwordEncoder, ContainerInterface $container)
	{
		$this->passwordEncoder = $passwordEncoder;
		$this->container = $container;
	}

    public function load(ObjectManager $manager)
    {
    	$companies = [];
	    $bilmo = (new Company())->setName('Bilmo')
		    ->setUpdatedAt(new DateTime());
	    $manager->persist($bilmo);
	    $superAdmin = (new User)
		    ->setCompany($bilmo)
		    ->setEmail($this->container->getParameter('app.admin_mail'))
		    ->setUsername($this->container->getParameter('app.admin_mail'))
		    ->setFirstname($this->container->getParameter('app.admin_firstname'))
		    ->setLastname($this->container->getParameter('app.admin_lastname'))
		    ->setRoles(["ROLE_USER", "ROLE_ADMIN", "ROLE_SUPER_ADMIN"])
		    ->setUpdatedAt(new DateTime())
	    ;
	    $manager->persist($superAdmin);
	    $superAdmin->setPassword($this->passwordEncoder->encodePassword(
			    $superAdmin,
		    $this->container->getParameter('app.admin_pass')
		    ));
	    $manager->persist($bilmo);

    	$faker = Factory::create();
    	for($i = 0 ; $i < 19 ; $i++){
		     $company = (new Company())
			     ->setName($faker->company)
			     ->setUpdatedAt(new DateTime());
		     $manager->persist($company);
		     $companies[] = $company;
	    }
    	foreach ($companies as $co){
    		$admin = (new User())
			    ->setFirstName('admin'.$company->getId())
			    ->setLastName('admin'.$company->getId())
			    ->setUserName('admin'.$company->getId().'@email.com')
			    ->setEmail('admin'.$company->getId().'@email.com')
			    ->setCompany($co)
			    ->setUpdatedAt(new DateTime());
		    $manager->persist($admin);
		    $admin->setPassword($this->passwordEncoder->encodePassword(
			    $admin,
			    $admin->getFirstName()
		    ));
		    $admin->setRoles((["ROLE_USER", "ROLE_ADMIN"]));
		    for ($i = 0; $i < 5; ++$i) {
			    $username = $faker->email;
			    $user = (new User())
			    ->setFirstName($faker->firstName)
			    ->setLastName($faker->lastName)
			    ->setUserName($username)
			    ->setEmail($username)
			    ->setCompany($co)
			    ->setUpdatedAt(new DateTime());
			$manager->persist($user);
			$user->setPassword($this->passwordEncoder->encodePassword(
				$user,
				$user->getFirstName()
			));
		    $user->setRoles($user->getRoles());
		}
	    }
        $manager->flush();
    }

	public static function getGroups(): array
	{
		return ['CompanyFixture'];
	}
}
